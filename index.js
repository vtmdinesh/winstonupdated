//const logger = require("./logger/logger.js")
const myLogger = require("./logger/timeStampLogger.js")


myLogger.info("first info log")
myLogger.error("error log")
myLogger.debug("lets debug")
myLogger.info("first info log")
myLogger.error("error log")
myLogger.debug("lets debug")

setInterval(() => {
    myLogger.log({
        level: 'info',
        message: "this is info log in async"
    })
}, 1000)

