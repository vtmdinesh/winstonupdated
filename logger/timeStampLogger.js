const winston = require('winston');
const { combine, timestamp, label, printf, colorize, prettyPrint } = winston.format;

const myFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} ${label} ${level}: ${message}`;
});

const myLogger = winston.createLogger({
    level: 'debug',
    format: combine(
        colorize(),
        label({ label: 'Sample logger' }),
        timestamp({ format: "HH:mm:ss" }),
        myFormat
    ),
    transports: [
        new winston.transports.File({ filename: "timeStampLogger.log" }),
        new winston.transports.Console(),
    ]
});



module.exports = myLogger